/*
5. Dana jest kolejka. Przy uŜyciu standardowych operacji zdefiniować operacje: 
· przesuwającą ostatnio wstawiony element na początek kolejki; 
· usuwającą ostatnio wstawiony do kolejki element; 
· odwracający kolejność elementów w kolejce.

Standardowe operacje przyjmuję jako te wymienione w zadaniu 3.

*/
kolejka * move_last_to_top (kolejka *queue){
	kolejka *temp = init();
	int c = count(queue);
	for (int i = 1; i < c; ++i)
		push(temp, pull(queue));
	for (int i = 1; i < c; ++i)
		push(queue, pull(temp));
	return queue;
}

kolejka * remove_last(kolejka *queue){
	kolejka *temp = init();
	int c = count(queue);
	for (int i = 1; i < c; ++i)
		push(temp, pull(queue));
	return temp;
}

kolejka * reverse(kolejka *queue){
	pelement p = NULL;
	if (count(queue) > 0){
		p = pull(queue);
		reverse(queue);
	}
	push(queue, p);
	return queue;
}
	
