/*3. Kolejkę zaimplementowano w postaci jednokierunkowego łańcucha odsyłaczowego. 
pelement = ^ element; 
element = record w : integer; nast : pelement; end;
 kolejka = record pierwszy,ostatni : pelement; end; 
Napisać funkcje: · 
inicjującą kolejkę; · 
zwracającą true jeŜeli kolejka jest pusta; · 
dołączającą element do kolejki; · 
pobierającą element z kolejki; · 
zwracającą liczbę elementów kolejki; · 
łączącą dwie kolejki w jedną.*/

#include <stdlib.h>
#include <stdio.h>
int main(){
	struct element{
		int w;
		struct element *nast;
	};
	
	typedef struct element * pelement;
	
	struct kolejka{
		pelement pierwszy;
		pelement ostatni;
	};
	typedef struct kolejka kolejka;
	
	kolejka * init(){
		kolejka *queue = malloc(sizeof(kolejka));
		if (queue)
			queue->pierwszy = queue->ostatni = NULL;
		
		return queue;
	}
	
	int isEmpty(kolejka *queue){
		if (!queue) return 1;
		if (queue->pierwszy == NULL)
			return 1;
		else 
			return 0;
	}
	//dodaj do kolejki
	void push(kolejka *queue, pelement element){
		if (!queue || !element) return;
		//kolejka nie jest pusta
		if (queue->ostatni){
			queue->ostatni->nast = element;
			queue->ostatni = element;
			queue->ostatni->nast = NULL;
		} else{ //kolejka pusta
			queue->pierwszy = queue->ostatni = element;
		}
	}
	//zdejmij z kolejki
	pelement pull(kolejka *queue){
		if (!queue) return NULL;
		if (isEmpty(queue)){
			return NULL;
		} else {
			//kolejka nie jest pusta
			pelement element = queue->pierwszy;
			queue->pierwszy = element->nast;
			//jeżeli usuwamy ostatni element to trzeba zaktualizować koniec kolejki
			if(queue->pierwszy == NULL){
				queue->ostatni = NULL;
			}
			return element;
		}
	}
	//zlicz ilość elementów
	int count(kolejka *queue){
		if (!queue)	return 0;
		
		int i = 1;
		
		if (isEmpty(queue))
			return 0;
		
		pelement head = queue->pierwszy;
		pelement tail = queue->ostatni;
		while (head != tail){
			++i;
			head = head->nast;
		}
		return i;
	}
	//dokleja wq2 do q1 i zwraca q1 - najprostsza możliwa wersja
	//chyba że któraś jest pusta - wtedy zwraca tę drugą;
	kolejka * join(kolejka *q1, kolejka *q2){
		if (!q1 || !q2)
			return NULL;
		if (isEmpty(q1)) return q2;
		if (isEmpty(q2)) return q1;
		
		q1->ostatni->nast = q1->pierwszy;
		q1->ostatni = q2->ostatni;
		return q1;
	}

//zadanie 5

kolejka * move_last_to_top (kolejka *queue){
	kolejka *temp = init();
	int c = count(queue);
	for (int i = 1; i < c; ++i)
		push(temp, pull(queue));
	for (int i = 1; i < c; ++i)
		push(queue, pull(temp));
	return queue;
}

kolejka * remove_last(kolejka *queue){
	kolejka *temp = init();
	int c = count(queue);
	for (int i = 1; i < c; ++i)
		push(temp, pull(queue));
	return temp;
}

kolejka * reverse(kolejka *queue){
	pelement p = NULL;
	if (count(queue) > 0){
		p = pull(queue);
		reverse(queue);
	}
	push(queue, p);
	return queue;
}
//koniec zadania 5


	//testowanie
	void print(kolejka *queue){
		if (!queue)	return;
		
		if (isEmpty (queue)){
			printf("Empty\n");
			return;
		}
		pelement head = queue->pierwszy;
		while (head != NULL){
			printf("%d ", head->w);
			head = head->nast;
		}
		printf("\n");
	}
	struct element a,b,c,d,e;
	a.w = 1;
	a.nast = NULL;
	b.w = 2;
	b.nast = NULL;
	c.w = 3;
	c.nast = NULL;
	d.w = 4;
	d.nast = NULL;
	e.w = 5;
	e.nast = NULL;
	kolejka *queue = init();
	kolejka *k;
	push (queue, &a);
	push (queue, &b);
	push (queue, &c);
	push (queue, &d);
	push (queue, &e);
	print(queue);
	printf("%d\n", count(queue));
	print(reverse(queue));
	print(k = remove_last(queue));
	print(queue);
	print(remove_last(queue));
	print(move_last_to_top (k));
}
