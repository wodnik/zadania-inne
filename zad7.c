/*
7. Napisać funkcję dokonującą numeryzacji klucza klucza fn(klucz:string):word metodą: 
· sumowania arytmetycznego; 
· obliczania funkcji xor.
*/
#include <stdio.h>
#include <limits.h>

int main(){
	
	//zakładam zakończenie klucza bajtem zerowym (jak w znormalizowanym stringu C)
	unsigned int fn_arytm(char* klucz){
		char* p = klucz;
		unsigned int wynik = 0;
		unsigned int temp = 0;
		int sint = sizeof( unsigned int);
		int i = 0;
		while (*p){
			temp <<= i*8;
			temp |=(unsigned int) *p;
			++i;
			if (i == sint){
				wynik = wynik + temp;
				i = 0;
			}
			++p;
		}
		return wynik;
	}
	
	unsigned int fn_xor(char* klucz){
		char* p = klucz;
		unsigned int wynik = 0;
		unsigned int temp = 0;
		int sint = sizeof(int);
		int i = 0;
		while (*p){
			temp <<= i*8;
			temp |=(unsigned int) *p;
			++i;
			if (i == sint){
				wynik = wynik ^ temp ;
				i = 0;
			}
			++p;
		}
		return wynik;
	}
	
	
	printf("%u\n", fn_arytm("Litwo, Ojczyzno moja"));
	printf("%u\n", fn_arytm("litwo, ojczyzno moja"));
	printf("%u\n", fn_arytm("asoeuf hasdufh a;w48 f4fg"));
	printf("%u\n", fn_arytm("asoeuf hasdufh a;w48 q4fz"));
	printf("\n%u\n", fn_xor("Litwo, Ojczyzno moja"));
	printf("%u\n", fn_xor("litwo, ojczyzno moja"));
	printf("%u\n", fn_xor("asoeuf hasdufh a;w48 f4fg"));
	printf("%u\n", fn_xor("asoeuf hasdufh a;w48 q4fz"));
	return 0;
}
