/*
9. Dane jest drzewo binarne. 
drzewo = ^ element; 
element = record w : integer; lewy, prawy : drzewo; end; 
Napisać operacje: 
· wypisującą zawartość drzewa; 
· usuwającą drzewo z pamięci. 
Napisać funkcje: 
· zwracającą liczbę węzłów w drzewie; 
· zwracającą liczbę liści w drzewie; 
· zwracającą wysokość drzewa; 
· liczbę będącą stosunkiem liczby liści do liczby wszystkich węzłów; 
Napisać powyŜsze funkcje bez uŜycia rekurencji.
*/


struct element{
	int w;
	struct element *left;
	struct element *right;
};

typedef struct element* pnode;


//funkcje pomocnicze
pnode min (pnode root)
{
	pnode t=root;
	while (t && t->left)
	{
	t=t->left;
	}
	return t;
}

pnode successor(pnode wezel){
	if (wezel->right)
		return min(wezel->right);
	
	pnode tmp=wezel->parent;
	while (tmp && wezel==tmp->right) {
		wezel=tmp;
		tmp=wezel->parent;
	}
	return tmp;
}

void print(pnode root){
	if (!root) return 0;
	pnode wsk=min(root);
	while (wsk && successor(wsk))
	{
		printf("%d ", wsk->w);
		wsk=successor(wsk);
	}
	printf("\n");
}

int nodes(pnode root){
	if (!root) return 0;
	pnode wsk=min(root);
	int licznik = 0;
	while (wsk && successor(wsk))
	{
		if (wsk->left || wsk->right) ++licznik;
		wsk=successor(wsk);
	}
	return licznik;
}

int leafs(pnode root){
	if (!root) return 0;
	pnode wsk=min(root);
	int licznik = 0;
	while (wsk && successor(wsk))
	{
		if (!(wsk->left || wsk->right)) ++licznik;
		wsk=successor(wsk);
	}
	return licznik;
}

int leafs_to_all(pnode root){
	if (!root) return 0;
	pnode wsk=min(root);
	int licznik = 0;
	while (wsk && successor(wsk))
	{
		++licznik;
		wsk=successor(wsk);
	}
	return leafs(root) / licznik;
}


