/*
8. Dana jest tablica rozproszona t:array[0..max-1] of element, gdzie element jest typu:
 element = record klucz : string; zajęty : boolean; end 
 Dostępne są funkcja numeryzacji klucza fn(klucz:string):integer oraz 
 funkcja rozpraszająca hash(kn,i:integer):integer. 
 Napisać funkcje: 
 · zwracającą liczbę wystąpień rekordu o zadanym kluczu; 
 · zwracającą numer działki tablicy w której występuje rekord o zadanym kluczu; 
 · zwracającą stopień wypełnienia tablicy.
*/

int liczba_wystapien(char* klucz){
	int c = 0;
	for (int i = 0; i < max; ++i){
		if (!(strcmp(t[i].klucz, klucz)))
			++i;
	}
	return i;
}

int numer_dzialki(char* klucz){
	return hash(klucz, max);
}
