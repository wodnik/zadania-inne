/*
Dany jest stos. Przy uŜyciu standardowych operacji zdefiniować operacje: 
· przesuwającą ostatnio wstawiony element na dno stosu; 
· usuwającą element z dna stosu; 
· odwracający kolejność elementów stosie.

zakładam dane operacje:
stack* init_stack(),  push(stack*, pelement), pelement pop(queue), count(queue), join(stack* s1, stack* s2)
operacja join zwraca połączenie w postaci s2 + s1 (na s2 wkładam s1), czyli wierzchołek stosu wynikowego to wierzchołek s1, spód stosu wynikowego to spód stosu s2
*/

stack* move_top_to_bottom(stack* s){
	stack* temp;
	temp = init_stack();
	push(temp, pop(s));
	return join(s, temp);
}

stack* remove_bottom(stack* s){
	stack* s2;
	s2 = init_stack();
	int c = count(s);
	for (int i = 1; i < c; ++i)
		push(s2, pop(s));
	pop (s);
	for (int i = 1; i < c; ++i)
		push(s, pop(s2));
	return s;
}

stack* reverse(stack* s){
	stack* s2 = init_stack();
	int c = count(s);
	for(int i = 0; i < c; ++i){
		push(s2, pop(s));
	}
	return s2;
}
